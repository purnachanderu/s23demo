variable "region" {
  default = "us-east-1"
}
variable "cidr" {
  type = map(string)
  default = {
    all      = "0.0.0.0/0"
    vpc_cidr = "10.0.0.0/16"
    pub_cidr = "10.0.16.0/24"
    pri_cidr = "10.0.32.0/24"
  }
}
variable "az" {
  type = map(string)
  default = {
    az1 = "us-east-1a"
    az2 = "us-east-1b"
    az3 = "us-east-1c"
  }
}
variable "ami" {
  default = "ami-03653b248ab5aa41a"
}
variable "key" {
  default = "linex"
}
variable "inst_type" {
  default = "t2.medium"
}
variable "nat_gateway" {
  default = "nat_gt"
}
variable "route_tb2" {
  default = "route_table2"
}
variable "aag_name" {
  default = "autoscaling_grp"
}
variable "tar_grp" {
  default = "targetgrp"
}
variable "asg_home" {
  default = "home_asg"
}
variable "asg_laptop" {
  default = "laptop_asg"
}
variable "asg_mobile" {
  default = "mobile_asg"
}
variable "tar_grp_home" {
  default = "home-tg"
}
variable "tar_grp_laptop" {
  default = "laptop-tg"
}
variable "tar_grp_mobile" {
  default = "mobile-tg"
}