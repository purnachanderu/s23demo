resource "aws_launch_configuration" "alc_home" {
  name            = "lc_home"
  image_id        = var.ami
  instance_type   = var.inst_type
  key_name        = var.key
  security_groups = [aws_security_group.asg2.id]
  user_data       = file("home.sh")
}
resource "aws_launch_configuration" "alc_laptop" {
  name            = "lc_laptop"
  image_id        = var.ami
  instance_type   = var.inst_type
  key_name        = var.key
  security_groups = [aws_security_group.asg2.id]
  user_data       = file("laptop.sh")
}
resource "aws_launch_configuration" "alc_mobile" {
  name            = "lc_mobile"
  image_id        = var.ami
  instance_type   = var.inst_type
  key_name        = var.key
  security_groups = [aws_security_group.asg2.id]
  user_data       = file("mobile.sh")
}



# resource "aws_autoscaling_group" "aag" {
#   name = var.aag_name 
#   max_size = 3
#   min_size = 2
#   desired_capacity = 2
#   launch_configuration = aws_launch_configuration.alc.id  
#   vpc_zone_identifier = [aws_subnet.public_sb.id]   
#   target_group_arns = [aws_lb_target_group.atg.arn]
# }

resource "aws_autoscaling_group" "aag_home" {
  name                 = var.asg_home
  max_size             = 5
  min_size             = 2
  desired_capacity     = 2
  launch_configuration = aws_launch_configuration.alc_home.id
  vpc_zone_identifier  = [aws_subnet.public_sb.id]
  #target_group_arns = [aws_lb_target_group.atg.arn]
}

resource "aws_autoscaling_group" "aag_laptop" {
  name                 = var.asg_laptop
  max_size             = 3
  min_size             = 1
  desired_capacity     = 2
  launch_configuration = aws_launch_configuration.alc_laptop.id
  vpc_zone_identifier  = [aws_subnet.public_sb.id]
  #target_group_arns = [aws_lb_target_group.atg.arn]
}

resource "aws_autoscaling_group" "aag_mobile" {
  name                 = var.asg_mobile
  max_size             = 3
  min_size             = 1
  desired_capacity     = 2
  launch_configuration = aws_launch_configuration.alc_mobile.id
  vpc_zone_identifier  = [aws_subnet.public_sb.id]
  #target_group_arns = [aws_lb_target_group.atg.arn]
}
# resource "aws_lb_target_group_attachment" "test" {
#   target_group_arn = aws_lb_target_group.atg.arn
#   target_id        = aws_instance.test.id
#   port             = 80
# }

resource "aws_autoscaling_attachment" "aaa_home" {
  alb_target_group_arn   = aws_lb_target_group.atg_home.id
  autoscaling_group_name = aws_autoscaling_group.aag_home.id
}
resource "aws_autoscaling_attachment" "aaa_laptop" {
  alb_target_group_arn   = aws_lb_target_group.atg_laptop.id
  autoscaling_group_name = aws_autoscaling_group.aag_laptop.id
}
resource "aws_autoscaling_attachment" "aaa_mobile" {
  alb_target_group_arn   = aws_lb_target_group.atg_mobile.id
  autoscaling_group_name = aws_autoscaling_group.aag_mobile.id
}