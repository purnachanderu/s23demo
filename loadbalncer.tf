resource "aws_lb_target_group" "atg_home" {
  name     = var.tar_grp_home
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id
  tags = {
    "Name" = "hometg"
  }
}

resource "aws_lb_target_group" "atg_laptop" {
  name     = var.tar_grp_laptop
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id
  tags = {
    "Name" = "laptoptg"
  }
}
resource "aws_lb_target_group" "atg_mobile" {
  name     = var.tar_grp_mobile
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.main.id
  tags = {
    "Name" = "mobiletg"
  }
}






resource "aws_alb" "applb" {
  name            = "application-loadbalancer"
  security_groups = [aws_security_group.asg1.id]
  subnets         = [aws_subnet.public_sb.id, aws_subnet.private_sb.id]
  tags = {
    "Name" = "app_lb"
  }
  load_balancer_type = "application"
}

resource "aws_alb_listener" "aalbl" {
  load_balancer_arn = aws_alb.applb.id
  #listener_arn = aws_alb_listener.aalbl.arn
  port     = 80
  protocol = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.atg_home.arn
  }
}

resource "aws_alb_listener_rule" "rl1" {
  #load_balancer_arn = aws_alb.applb.id
  listener_arn = aws_alb_listener.aalbl.arn
  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.atg_laptop.arn
  }
  condition {
    path_pattern {
      values = ["/laptop/*"]
    }
  }
}

resource "aws_alb_listener_rule" "rl2" {
  #load_balancer_arn = aws_alb.applb.id
  listener_arn = aws_alb_listener.aalbl.arn
  action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.atg_mobile.arn
  }
  condition {
    path_pattern {
      values = ["/mobile/*"]
    }
  }
}