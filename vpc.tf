resource "aws_vpc" "main" {
  cidr_block = var.cidr["vpc_cidr"]
  tags = {
    Name = "myvpc_001"
  }
}
resource "aws_subnet" "public_sb" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.cidr["pub_cidr"]
  availability_zone = var.az["az1"]
  tags = {
    Name = "public_subnet"
  }
  map_public_ip_on_launch = true
}
resource "aws_subnet" "private_sb" {
  vpc_id            = aws_vpc.main.id
  cidr_block        = var.cidr["pri_cidr"]
  availability_zone = var.az["az2"]
  tags = {
    Name = "private_subnet"
  }
  #map_public_ip_on_launch = true
}
resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "igw"
  }
}
resource "aws_route_table" "art" {
  vpc_id = aws_vpc.main.id
  tags = {
    Name = "mai_rt"
  }
}
resource "aws_route_table_association" "arta" {
  subnet_id      = aws_subnet.public_sb.id
  route_table_id = aws_route_table.art.id
}

resource "aws_route" "r" {
  route_table_id         = aws_route_table.art.id
  destination_cidr_block = var.cidr["all"]
  gateway_id             = aws_internet_gateway.gw.id
}
resource "aws_eip" "aeip" {
  tags = {
    "Nmae" = "awseip"
  }
}
resource "aws_nat_gateway" "ang" {
  allocation_id = aws_eip.aeip.id
  subnet_id     = aws_subnet.public_sb.id
  tags = {
    "Name" = var.nat_gateway
  }
}
resource "aws_route_table" "art2" {
  vpc_id = aws_vpc.main.id
  tags = {
    "Name" = var.route_tb2
  }
  route {
    cidr_block     = var.cidr["all"]
    nat_gateway_id = aws_nat_gateway.ang.id
  }
}

resource "aws_route_table_association" "arta2" {
  subnet_id      = aws_subnet.private_sb.id
  route_table_id = aws_route_table.art2.id #aws_route_table.id.art2.id 
}